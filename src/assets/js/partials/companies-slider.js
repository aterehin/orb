/**
 * Companies Slider
 */

(function() {

	"use strict";

    var $slider = $('.companies');

	$slider.owlCarousel({
		loop: true,
		items: 3,
		margin: 11,
		nav: true,
		navText: ['<i class="icon-nav-prev"></i>', '<i class="icon-nav-next"></i>'],
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 2
            },
            992: {
                items: 3
            }
        }
	});

})();
