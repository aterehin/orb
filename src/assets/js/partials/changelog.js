/**
 * Changelog
 */

(function() {

	"use strict";

    var $btn = $('[data-change-log-toggle]');
	var $body = $('[data-change-log-body]');

	var classes = {
		visible: '__visible',
		blue: '__blue',
		red: '__red'
	};

    var texts = {
        'hidden': 'Previous versions',
        'visible': 'Hide versions'
    };

	$btn.on('click', handleClick);

	function handleClick() {
		var $this = $(this);
        var thisText = $this.text();

		$body.toggleClass(classes.visible);
		$this.toggleClass(classes.blue + ' ' + classes.red);
        $this.text( thisText == texts.hidden ? texts.visible : texts.hidden  );

		return false;
	}


})();
