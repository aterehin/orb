/**
 * Docs Parametrs
 */

(function() {

	"use strict";

    var $btn = $('.button[data-toggle]');
	var $item = $('.docs_item[data-toggle-id]');

	var classes = {
		active: '__active',
		visible: '__visible',
		blue: '__blue',
		red: '__red'
	};

	$btn.on('click', handleClick);

	function handleClick() {
		var $this = $(this);
		var item_id = $this.data('toggle');
		var entry_id = $this.data('toggle-entry');
		var $_item = $item.filter('[data-toggle-id="' + item_id + '"]');

		if($_item.hasClass(classes.active) && $this.hasClass(classes.red)) {
			$_item
				.find($btn)
				.removeClass(classes.red)
				.addClass(classes.blue);
			$_item
				.removeClass(classes.active);
		} else {
			$_item
				.find($btn)
				.removeClass(classes.red)
				.addClass(classes.blue)
				.filter(this)
				.removeClass(classes.blue)
				.addClass(classes.red);

			$_item
				.addClass(classes.active)
				.find('[data-entry-id]')
				.removeClass(classes.visible)
				.filter('[data-entry-id="' + entry_id + '"]')
				.addClass(classes.visible);
		}


		return false;
	}

})();
