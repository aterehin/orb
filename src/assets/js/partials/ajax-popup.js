/**
 * Ajax Popup
 */

(function() {

	"use strict";

    var $btn = $('[data-ajax-popup]');

	var options = {
		fancybox: {
			type: 'ajax',
			fitToView: false,
			padding: 0,
			afterShow: handlePopupLoad
		},
		validate: {
			submitHandler: handleFormSubmit
		},
		ajaxForm: {
			success: handleAjaxSuccess
		}
	}

	$btn.fancybox(options.fancybox);

	function handlePopupLoad(a, b) {
		var $form = $('.ajax-popup form');

		$form.validate(options.validate);
	}

	function handleFormSubmit(form) {
		var $form = $(form);

		$.fancybox.showLoading();
		$form.ajaxSubmit(options.ajaxForm);
	}

	function handleAjaxSuccess(response) {
		var $popup = $('.ajax-popup');

		$.fancybox.hideLoading();
		$popup.replaceWith(response);
	}
	
})();
