/**
 * Clear Input
 */

(function () {

    "use strict";

    var $banner = $('[data-action="fixed"]');
    var offset = $banner.offset().top;

    $(window).on('scroll load', function(){
        if (offset - $(this).scrollTop() <= 40) {
            fix($banner)
        } else {
            unFix($banner)
        }
    });

    function fix(el) {
        el.css({
            "position": "fixed",
            "width": "243px",
            "top": "40px"
        });
    }

    function unFix(el) {
        el.css({
            "position": "relative",
            "width": "auto",
            "top": 0
        })
    }

})();
