/**
 * Third party
 */
//= ../../../bower_components/jquery/dist/jquery.min.js
//= ../../../bower_components/owl.carousel/dist/owl.carousel.min.js
//= ../../../bower_components/fancybox/source/jquery.fancybox.pack.js
//= ../../../bower_components/jquery-validation/dist/jquery.validate.min.js
//= ../../../bower_components/jquery-form/jquery.form.js

 /**
 * Custom
 */
jQuery(document).ready(function($) {

    //= partials/companies-slider.js
    //= partials/docs-parametrs.js
    //= partials/ajax-popup.js
    //= partials/mobile-menu.js
    //= partials/changelog.js
    //= partials/fix-banner.js
});
