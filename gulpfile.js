var gulp     = require('gulp');
var prefixer = require('gulp-autoprefixer');
var cssmin   = require('gulp-cssmin');
var gcmq     = require('gulp-group-css-media-queries');
var imagemin = require('gulp-imagemin');
var notify   = require('gulp-notify');
var nunjucks = require('gulp-nunjucks-render');
var plumber  = require('gulp-plumber');
var prettify = require('gulp-prettify');
var replace  = require('gulp-replace');
var rigger   = require('gulp-rigger');
var sass     = require('gulp-sass');
var uglify   = require('gulp-uglify');
var spritesmith = require('gulp.spritesmith');
var buffer   = require('vinyl-buffer');

var paths = {
  src: {
    directories: './src/viewer/**/*',
    html: ['./src/*.html', './src/ajax/*.html'],
    templates: './src/templates/',
    img: [
      './src/assets/img/**/*.{gif,jpg,png,svg}',
      '!./src/assets/img/sprites/*.png'
    ],
    sprites: './src/assets/img/sprites/*.png',
    sprites2x: './src/assets/img/sprites/*@2x.png',
    scss: './src/assets/scss/styles.scss',
    js: './src/assets/js/scripts.js'
  },
  dest: {
    directories: './build/viewer/',
    html: './build/',
    img: './build/assets/img/',
    css: './build/assets/css/',
    sprites: './src/assets/scss/utils/',
    fonts: './build/assets/fonts/',
    js: './build/assets/js/'
  },
  watch: {
    html: './src/**/*.html',
    img: './src/assets/img/**/*.{gif,jpg,png,svg}',
    scss: './src/assets/scss/**/*.scss',
    js: './src/assets/js/**/*.js'
  }
};

var plumberErrorHandler = {
  errorHandler: notify.onError({
    title: 'Gulp',
    message: 'Error: <%= error.message %>'
  })
};

gulp.task('copy', function () {
  // HTML pages
  gulp.src(paths.src.directories)
      .pipe(gulp.dest(paths.dest.directories))
});

gulp.task('html', function () {
  // HTML pages
  gulp.src(paths.src.html, { base: './src' })
      .pipe(plumber(plumberErrorHandler))
      .pipe(nunjucks({
        path: paths.src.templates
      }))
      .pipe(prettify({
        indent_inner_html: false,
        unformatted: [],
        extra_liners: []
      }))
      .pipe(gulp.dest(paths.dest.html))
});

gulp.task('img', function () {
  // All images
  gulp.src(paths.src.img)
      .pipe(buffer())
      .pipe(imagemin({
        optimizationLevel: 7,
        progressive: true,
        interlaced: true
      }))
      .pipe(gulp.dest(paths.dest.img));

  // Sprites
  var spritesData =
    gulp.src(paths.src.sprites)
        .pipe(plumber(plumberErrorHandler))
        .pipe(spritesmith({
          retinaSrcFilter: paths.src.sprites2x,
          imgName: 'sprite.png',
          retinaImgName: 'sprite@2x.png',
          cssName: '_sprites.scss',
          imgPath: '../img/sprite.png',
          retinaImgPath: '../img/sprite@2x.png',
          cssTemplate: 'sprites.template.handlebars'
        }));

  // Sprite images
  spritesData.img
    .pipe(buffer())
    .pipe(imagemin({
      optimizationLevel: 7,
      progressive: true,
      interlaced: true
    }))
    .pipe(gulp.dest(paths.dest.img));

  // Sprite css
  spritesData.css
    .pipe(gulp.dest(paths.dest.sprites));
});

gulp.task('css', function () {
  // Main styles
  gulp.src(paths.src.scss)
      .pipe(plumber(plumberErrorHandler))
      .pipe(sass())
      .pipe(prefixer())
      .pipe(gcmq())
      .pipe(cssmin({
        keepSpecialComments: 0
       }))
      .pipe(replace(/url\((..\/img\/)?/g, 'url(../img/'))
      .pipe(gulp.dest(paths.dest.css));
});

gulp.task('js', function () {
  // Main scripts
  gulp.src(paths.src.js)
      .pipe(plumber(plumberErrorHandler))
      .pipe(rigger())
      .pipe(uglify())
      .pipe(gulp.dest(paths.dest.js));
});

gulp.task('watch', function() {
  gulp.watch(paths.watch.html, ['html']);
  gulp.watch(paths.watch.img, ['img']);
  gulp.watch(paths.watch.scss, ['css']);
  gulp.watch(paths.watch.js, ['js']);
});

gulp.task('default', ['copy', 'html', 'img', 'css', 'js', 'watch']);
